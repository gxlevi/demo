package com.aop.secret.controller;

import com.aop.secret.annotation.Secret;
import com.aop.secret.pojo.UserInfo;
import com.aop.secret.pojo.UserInfoReq;
import com.aop.secret.util.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 *   一个注解搞定SpringBoot接口定制属性加解密
 * @author 蜗牛
 */
@RestController
@RequestMapping("/api/aes")
@Slf4j
public class AesTestController {

	@GetMapping("/getUser")
	@Secret(reqPropsName = {}, respPropsName = {"phone", "idCard", "name"})
	public Result<UserInfo> getUser() {

		// 模拟返回用户信息
		UserInfo userInfo = new UserInfo();
		userInfo.setId(1001L);
		userInfo.setName("蜗牛");
		userInfo.setAge(500);
		userInfo.setIdCard("320125199801253349");
		userInfo.setPhone("15912345678");
		userInfo.setAddress("蜗牛洞");

		return new Result(HttpStatus.OK.value(), "查询成功", userInfo);
	}

	@PostMapping("/saveUser")
	@Secret(reqPropsName = {"phone", "idCard", "name"}, respPropsName = {})
	public Result saveUser(@RequestBody UserInfoReq userInfoReq) {
		return new Result(HttpStatus.OK.value(), "保存成功", userInfoReq);
	}
}
